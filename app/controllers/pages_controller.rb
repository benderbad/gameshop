class PagesController < ApplicationController
  def rule
    @page = Page.find_by(url: 'rules')
  end
  def payment
    @page = Page.find_by(url: 'payment')
  end
  def help
    @page = Page.find_by(url: 'help')
  end
  def user
    @user = User.find(params[:id])
  end
  def balance
    unless params['code'].nil?
      coupon = Coupon.find_by(code: params['code'])
      if coupon && coupon.active
        coupon.active = false
        coupon.save
        current_user.balance += coupon.value
        current_user.save
        flash[:success] = "Купон активирован!"
      else
        flash[:alert] = "Код купона не верен!"
      end
      redirect_to '/users/balance/'
    end
  end

  def configuration
    redirect_to '/' unless (current_user && current_user.admin?)
    @coupon = Coupon.new
  end
end
