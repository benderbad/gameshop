RailsAdmin.config do |config|

  config.parent_controller = "::ApplicationController"
  config.authorize_with do |controller|
    unless current_user && current_user.admin?
      redirect_to('/', alert: "You are not permitted to view this page")
    end
  end
  config.included_models = [ User, Buyer, Seller, Admin, Product, Account, Coupon, Page]
  config.model Product do
    edit do
      configure :descriptoin, :ck_editor
    end
  end
  config.model Page do
    edit do
      configure :desc, :ck_editor
    end
  end
  config.model Account do
      edit do
       configure :password, :string
      end
      field :title
      field :email
      field :seller
      field :buyer do
        required false
      end
      field :product
      field :price
  end
  config.model Buyer do
    parent User
    label_plural 'Buyers'
  end
  config.model Seller do
    parent User
    label_plural 'Sellers'
  end
  config.model Admin do
    parent User
    label_plural 'Admins'
  end
  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
  end
  
end
