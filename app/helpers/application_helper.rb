module ApplicationHelper
    def message_count
        Message.where(sender_id: current_user.id).where(sender: false).count + Message.where(resender_id: current_user.id).where(resender: false).count
    end

    def get_message(id)
        con = Conversation.find(id)
        if con.sender_id == current_user.id
            "#{con.messages.where(sender_id: current_user.id).where(sender: false).count} / #{con.messages.count}"
        elsif con.reender_id == current_user.id
            "#{con.messages.where('resender_id=? AND resender=?', current_user.id, false).count} / #{con.messages.count}"
        end
    end

    def sender_account_sold(id)
        Account.where(seller_id: @user.id).where(buyer_id: nil).count
    end

    def sender_account_all(id)
        Account.where(seller_id: @user.id).count
    end

    def buyer_account id
        Account.where(buyer_id: @user.id).count
    end
end
