class User < ApplicationRecord

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :accounts

  after_create :default_type
  
  def default_type
    self.type = 'Buyer'
  end

  def admin?
  	type == 'Admin'
  end

  def seller?
  	type == 'Seller'
  end

  def buyer?
  	type == 'Buyer'
  end
  
end
