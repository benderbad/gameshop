class Product < ApplicationRecord
  validates :name, presence: true
  validates :descriptoin, presence: true
  mount_uploader :photo, PhotoUploader
  serialize :photo, JSON
  has_many :accounts
end
