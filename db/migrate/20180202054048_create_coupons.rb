class CreateCoupons < ActiveRecord::Migration[5.1]
  def change
    create_table :coupons do |t|
      t.string :code
      t.integer :value
      t.boolean :active, default: true
      t.timestamps
    end
  end
end
