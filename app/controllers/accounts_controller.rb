class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy]
  # load_and_authorize_resource
  # GET /accounts
  # GET /accounts.json
  def index
    if current_user.admin?
      @accounts = Account.all
    elsif current_user.seller?
      @accounts = Account.where(seller_id: current_user.id)
    elsif current_user.buyer?
      @accounts = Account.where(buyer_id: current_user.id)
    end
    @account = Account.new
  end

  def pay
    account = Account.find(params['id'])
    if current_user.balance >= account.price
      account.buyer_id = current_user.id
      account.save
      current_user.balance -= account.price
      current_user.save
      flash[:success] = "Аккаунт успешно куплен!"
      redirect_to '/accounts'
    else
      flash[:alert] = "На вашем счете недостаточно средств!"
      redirect_to params['url']
    end
  end
  # GET /accounts/1
  # GET /accounts/1.json
  def show
  end

  # GET /accounts/new
  def new
    @accounts = Account.where(seller_id: current_user.id)
    @account = Account.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(account_params)

    respond_to do |format|
      if @account.save
        format.html { redirect_to '/accounts', notice: 'Account was successfully created.' }
        format.json { render :index, status: :created, location: @account }
      else
        format.html { render :index }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    @account.seller_id = current_user.id
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to '/accounts', notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to '/accounts', notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:title, :email, :password, :seller_id, :buyer_id, :price, :product_id)
    end
end
