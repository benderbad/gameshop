class Conversation < ApplicationRecord
    has_many :messages
    accepts_nested_attributes_for :messages
    validates  :title, presence: true
end
