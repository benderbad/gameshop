class AddProductBuyer < ActiveRecord::Migration[5.1]
  def change
  	add_column :products, :buyer_id, :integer
  	add_column :products, :photo, :text
  end
end
