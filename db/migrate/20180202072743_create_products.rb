class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.text :descriptoin
      t.float :price
      t.references :user, foreign_key: true
      t.integer :pcs
      t.timestamps
    end
  end
end
