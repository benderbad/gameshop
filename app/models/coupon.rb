class Coupon < ApplicationRecord
	validates :code, presence: true, uniqueness: true
	validates :value, presence: true
end
