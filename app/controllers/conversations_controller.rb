class ConversationsController < ApplicationController
  before_action :set_conversation, only: [:show, :edit, :update, :destroy]

  # GET /conversations
  # GET /conversations.json
  def index
    @conversations = Conversation.where('sender_id=? OR reender_id=?', current_user.id, current_user.id)
  end

  # GET /conversations/1
  # GET /conversations/1.json
  def show
    conv = Conversation.find(params['id'])
    
    if (conv.sender_id == current_user.id )|| (conv.reender_id == current_user.id)
      @messages = conv.messages
      @messages.each do |f|
        if f.sender_id == current_user.id
          f.sender = true
        else
          f.resender = true
        end
        f.save
      end
    
    else
      redirect_to conversation_params
    end
  end

  # GET /conversations/new
  def new
    @conversation = Conversation.new
    @conversation.messages.build
  end

  # GET /conversations/1/edit
  def edit
  end

  # POST /conversations
  # POST /conversations.json
  def create
    @conversation = Conversation.new(conversation_params)

    respond_to do |format|
      if @conversation.save
        format.html { redirect_to @conversation, notice: 'Conversation was successfully created.' }
        format.json { render :show, status: :created, location: @conversation }
      else
        format.html { render :new }
        format.json { render json: @conversation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /conversations/1
  # PATCH/PUT /conversations/1.json
  def update
    respond_to do |format|
      if @conversation.update(conversation_params)
        format.html { redirect_to @conversation, notice: 'Conversation was successfully updated.' }
        format.json { render :show, status: :ok, location: @conversation }
      else
        format.html { render :edit }
        format.json { render json: @conversation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conversations/1
  # DELETE /conversations/1.json
  def destroy
    @conversation.destroy
    respond_to do |format|
      format.html { redirect_to conversations_url, notice: 'Conversation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conversation
      @conversation = Conversation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conversation_params
      params.require(:conversation).permit(:title, :sender_id, :reender_id, messages_attributes: [:text, :conversation_id, :sender_id, :resender_id, :sender, :resender])
    end
end
