json.extract! account, :id, :email, :password, :seller_id, :buyer_id, :price, :product, :created_at, :updated_at
json.url account_url(account, format: :json)
