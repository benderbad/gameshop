set :application, 'gameshop'
set :repo_url, "git@bitbucket.org:benderbad/gameshop.git"
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }
set :unicorn_config_path, "var/www/gameshop/current/config/unicorn.rb"
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')
namespace :deploy do
  task :setup do
    before "deploy:migrate", :create_db
    invoke :deploy
  end
  task :create_db do
    on roles(:all) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "db:create"
        end
      end
    end
  end
  task :restart do
    invoke 'unicorn:legacy_restart'
  end
end
# before :deploy, 'git:push'
# before 'deploy:setup', 'git:push'