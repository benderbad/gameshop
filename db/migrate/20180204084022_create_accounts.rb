class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.string :title
      t.string :email
      t.string :password
      t.integer :seller_id
      t.integer :buyer_id
      t.float :price
      t.integer :product_id

      t.timestamps
    end
  end
end
