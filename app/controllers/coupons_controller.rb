class CouponsController < ApplicationController
  before_action :set_coupon, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
    
  def index
    @coupons = Coupon.all
  end

  def show
  end

  def new
    @coupon = Coupon.new
  end

  def edit
  end

  def create
    unless params['coupon']['pcs'].nil?
      pcs = params['coupon']['pcs']
      value = params['coupon']['value']
      (1..pcs.to_i).each do |f|
        Coupon.create(code: [*('a'..'z'),*('0'..'9'),*('A'..'Z')].shuffle[0,10].join, value: value, active: true)
      end      
    end
    respond_to do |format|
      if @coupon.save
        format.html { redirect_to '/configuration', notice: 'Coupon was successfully created.' }
        format.json { render '/configuration', status: :created, location: '/configuration' }
      else
        format.html { redirect_to '/configuration' }
        format.json { render json: @coupon.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @coupon.update(coupon_params)
        format.html { redirect_to @coupon, notice: 'Coupon was successfully updated.' }
        format.json { render :show, status: :ok, location: @coupon }
      else
        format.html { render :edit }
        format.json { render json: @coupon.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @coupon.destroy
    respond_to do |format|
      format.html { redirect_to coupons_url, notice: 'Coupon was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_coupon
      @coupon = Coupon.find(params[:id])
    end

    def coupon_params
      params.require(:coupon).permit(:code, :value, :active)
    end
end
