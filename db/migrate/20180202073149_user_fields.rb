class UserFields < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :balance, :float, default: 0
  	add_column :users, :type,    :string
  	add_column :users, :user_id, :integer
  	add_column :users, :percent, :float, default: 0
  end
end
