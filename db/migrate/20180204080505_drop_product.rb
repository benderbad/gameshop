class DropProduct < ActiveRecord::Migration[5.1]
  def change
    remove_column :products, :price
    remove_column :products, :user_id
    remove_column :products, :buyer_id        
  end
end
