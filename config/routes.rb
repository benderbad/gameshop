Rails.application.routes.draw do
  resources :conversations
  resources :accounts
  resources :products
  resources :coupons
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  mount Ckeditor::Engine => '/ckeditor'
  get '/rules',  to: 'pages#rule'
  get '/help',  to: 'pages#help'
  get '/payment',  to: 'pages#payment'
  get '/configuration',  to: 'pages#configuration'
  get '/users/balance', to: 'pages#balance'
  get '/account/pay',   to: 'accounts#pay'
  get '/users/:id',     to: 'pages#user'
  root to: 'products#index'
end
