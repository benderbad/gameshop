class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.text :text
      t.references :conversation, foreign_key: true
      t.integer :sender_id
      t.integer :resender_id
      t.boolean :sender, default: false
      t.boolean :resender, default: false
      t.timestamps
    end
  end
end
